import { AsyncSeriesWaterfallHook } from "tapable";

export interface BaseScraperHook<T, U> {
  before: AsyncSeriesWaterfallHook<U>;
  after: AsyncSeriesWaterfallHook<[T, U]>;
}

export interface BaseScraper<T, U> {
  scrapHooks: BaseScraperHook<T, U>;
  scrap(rlt: T, ctx: U): Promise<T>;
}
