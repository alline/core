import { AsyncSeriesWaterfallHook } from "tapable";

import { EpisodeContext, EpisodeResult } from "../type";

import { BaseScraper, BaseScraperHook } from "./base";

export abstract class EpisodeScraper
  implements BaseScraper<EpisodeResult, EpisodeContext> {
  scrapHooks: BaseScraperHook<EpisodeResult, EpisodeContext>;

  constructor() {
    this.scrapHooks = {
      before: new AsyncSeriesWaterfallHook(["ctx"]),
      after: new AsyncSeriesWaterfallHook(["rlt", "ctx"])
    };
  }

  async scrap(rlt: EpisodeResult, ctx: EpisodeContext): Promise<EpisodeResult> {
    const { before, after } = this.scrapHooks;
    const newCtx = await before.promise(ctx);
    const result = await this.onScrap(rlt, newCtx);
    return after.promise(result, ctx);
  }

  protected abstract onScrap(
    rlt: EpisodeResult,
    ctx: EpisodeContext
  ): Promise<EpisodeResult>;
}
