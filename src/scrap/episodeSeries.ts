import { createLogger, format, Logger, transports } from "winston";
import _ from "lodash";
import { Episode } from "@alline/model";

import { EpisodeContext, EpisodeResult } from "../type";
import { BaseSerializer } from "../serialize";

import { EpisodeScraper } from "./episode";

const createDefaultLogger = (level: string): Logger =>
  createLogger({
    level,
    format: format.combine(
      format.colorize({ level: true }),
      format.timestamp(),
      format.printf(
        ({ timestamp, level, label = "default", message, ...other }) => {
          const otherStr = _.isEmpty(other)
            ? ""
            : ` (${JSON.stringify(other)})`;
          return `${timestamp} [${level}] <${label}> ${message}${otherStr}`;
        }
      )
    ),
    transports: [new transports.Console()]
  });

export interface EpisodeSeriesScraperOption {
  logger?: Logger;
  logLevel?: string;
}

export interface EpisodeSeriesScrapOption {
  scrapers: EpisodeScraper[];
  title: string;
  season: number;
  episodes: {
    from: number;
    to: number;
  };
  serializers: {
    data: BaseSerializer<Episode, EpisodeContext>;
    image: BaseSerializer<string[], EpisodeContext>;
  };
}

export class EpisodeSeriesScraper {
  protected logger: Logger;

  constructor(option: EpisodeSeriesScraperOption = {}) {
    const {
      logLevel = process.env.NODE_ENV === "production" ? "info" : "debug",
      logger = createDefaultLogger(logLevel)
    } = option;
    this.logger = logger;
  }

  async scrap(option: EpisodeSeriesScrapOption): Promise<void> {
    const { scrapers, title, episodes, season, serializers } = option;
    const { data: dataSerializer, image: imageSerializer } = serializers;
    const scrapEpisodes = _.range(episodes.from, episodes.to).map(
      async episode => {
        const ctx: EpisodeContext = {
          title,
          season,
          episode,
          logger: this.logger
        };
        const { data, thumbnails } = await this.scrapEpisode(ctx, scrapers);
        await Promise.all([
          dataSerializer.serialize(data, ctx),
          imageSerializer.serialize(thumbnails, ctx)
        ]);
      }
    );
    await Promise.all(scrapEpisodes);
  }

  private async scrapEpisode(
    ctx: EpisodeContext,
    scrapers: EpisodeScraper[]
  ): Promise<EpisodeResult> {
    const episode: Episode = {
      title: [],
      contentRating: "",
      directors: [],
      writers: []
    };
    const initial: EpisodeResult = {
      data: episode,
      thumbnails: []
    };
    return scrapers.reduce(async (result, scraper) => {
      const episode = await result;
      return scraper.scrap(episode, ctx);
    }, Promise.resolve(initial));
  }
}
