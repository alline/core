import { Episode } from "@alline/model";
import { Logger } from "winston";

export interface EpisodeContext {
  title: string;
  season: number;
  episode: number;
  logger: Logger;
}

export interface EpisodeResult {
  data: Episode;
  thumbnails: string[];
}
