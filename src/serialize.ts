import { AsyncSeriesHook, AsyncSeriesWaterfallHook } from "tapable";

export interface BaseSerializerHook<T, U> {
  before: AsyncSeriesWaterfallHook<[T, U]>;
  after: AsyncSeriesHook<[T, U]>;
}

export interface BaseSerializer<T, U> {
  serializeHooks: BaseSerializerHook<T, U>;
  serialize(data: T, ctx: U): Promise<void>;
}

export abstract class ModelSerializer<T, U> implements BaseSerializer<T, U> {
  serializeHooks: BaseSerializerHook<T, U>;

  constructor() {
    this.serializeHooks = {
      before: new AsyncSeriesWaterfallHook(["data", "ctx"]),
      after: new AsyncSeriesHook(["data", "ctx"])
    };
  }

  async serialize(data: T, ctx: U): Promise<void> {
    const { before, after } = this.serializeHooks;
    const newData = await before.promise(data, ctx);
    await this.onSerialize(newData, ctx);
    await after.promise(data, ctx);
  }

  protected abstract onSerialize(data: T, ctx: U): Promise<void>;
}

export interface BaseDeserializerHook<T, U> {
  before: AsyncSeriesHook<[U]>;
  after: AsyncSeriesWaterfallHook<[T, U]>;
}

export interface BaseDeserializer<T, U> {
  deserializeHooks: BaseDeserializerHook<T, U>;
  deserialize(ctx: U): Promise<T>;
}

export abstract class ModelDeserializer<T, U>
  implements BaseDeserializer<T, U> {
  deserializeHooks: BaseDeserializerHook<T, U>;

  constructor() {
    this.deserializeHooks = {
      before: new AsyncSeriesHook(["ctx"]),
      after: new AsyncSeriesWaterfallHook(["data", "ctx"])
    };
  }

  async deserialize(ctx: U): Promise<T> {
    const { before, after } = this.deserializeHooks;
    await before.promise(ctx);
    const data = await this.onDeserialize(ctx);
    return after.promise(data, ctx);
  }

  protected abstract onDeserialize(ctx: U): Promise<T>;
}
