# [3.2.0](https://gitlab.com/alline/core/compare/v3.1.0...v3.2.0) (2020-07-10)


### Features

* update serializer ([26f83f2](https://gitlab.com/alline/core/commit/26f83f2e66d57714a280070054334ab274aa4611))

# [3.1.0](https://gitlab.com/alline/core/compare/v3.0.2...v3.1.0) (2020-07-09)


### Features

* move utility functions to @alline/util ([342252a](https://gitlab.com/alline/core/commit/342252ac576079d0406cd6e4b780c85d6a979140))

## [3.0.2](https://gitlab.com/alline/core/compare/v3.0.1...v3.0.2) (2020-07-09)


### Bug Fixes

* serializer parameters ([f5e5c2d](https://gitlab.com/alline/core/commit/f5e5c2d593f69c85ad6667d093439c2e9719ab2d))

## [3.0.1](https://gitlab.com/alline/core/compare/v3.0.0...v3.0.1) (2020-07-09)


### Bug Fixes

* interface name ([1a381f5](https://gitlab.com/alline/core/commit/1a381f58a46b3513afd29a0665bbfe659b0b0323))

# [3.0.0](https://gitlab.com/alline/core/compare/v2.4.0...v3.0.0) (2020-07-09)


### Bug Fixes

* update dependencies ([9e8a10a](https://gitlab.com/alline/core/commit/9e8a10a58cb25e9472d564473b9da528f0aeab93))


### Code Refactoring

* clean up api to be more consistent ([f243d7c](https://gitlab.com/alline/core/commit/f243d7c030c54d38bea487f70c88132ddeea43ad))


### BREAKING CHANGES

* change api

# [2.4.0](https://gitlab.com/alline/core/compare/v2.3.4...v2.4.0) (2020-07-02)


### Features

* add utility funcitons ([0e0f03e](https://gitlab.com/alline/core/commit/0e0f03e8ae0c1fb4645e9173f024f2484c3a7d1b))

## [2.3.4](https://gitlab.com/alline/core/compare/v2.3.3...v2.3.4) (2020-06-29)


### Bug Fixes

* lint problems ([e54f0f8](https://gitlab.com/alline/core/commit/e54f0f8d0fb633487183752a055a72d86e29a49f))

## [2.3.3](https://gitlab.com/alline/core/compare/v2.3.2...v2.3.3) (2020-06-29)


### Bug Fixes

* add default option for EpisodeSeriesScraper ([06f4402](https://gitlab.com/alline/core/commit/06f440219ae554314d0af93922afef1ad283bda0))

## [2.3.2](https://gitlab.com/alline/core/compare/v2.3.1...v2.3.2) (2020-06-29)


### Bug Fixes

* missing export ([fc55cf5](https://gitlab.com/alline/core/commit/fc55cf52e87258dc77a1451f063af32196c99334))

## [2.3.1](https://gitlab.com/alline/core/compare/v2.3.0...v2.3.1) (2020-06-29)


### Bug Fixes

* update dependencies ([2961d56](https://gitlab.com/alline/core/commit/2961d5657b2bd05bf51d0b5df85cb8ca8cd13a3f))

# [2.3.0](https://gitlab.com/alline/core/compare/v2.2.0...v2.3.0) (2020-05-19)


### Features

* add context for hooks ([ab6cf77](https://gitlab.com/alline/core/commit/ab6cf776ef3e4f07a81ed7a8123da22744cffbed))

# [2.2.0](https://gitlab.com/alline/core/compare/v2.1.0...v2.2.0) (2020-05-19)


### Features

* allow hooks to be extended ([b8dd7b5](https://gitlab.com/alline/core/commit/b8dd7b51cc2a4d7728196f10c7fd908b8eeff410))

# [2.1.0](https://gitlab.com/alline/core/compare/v2.0.0...v2.1.0) (2020-05-18)


### Features

* add log level ([e0a807b](https://gitlab.com/alline/core/commit/e0a807b2d99db2bf6c16a3e22214f0ceafa0335b))

# [2.0.0](https://gitlab.com/alline/core/compare/v1.1.0...v2.0.0) (2020-05-18)


### Bug Fixes

* remove createDefaultLogger ([b41409e](https://gitlab.com/alline/core/commit/b41409e685bff461b4ef6276a2efd39bf45859e7))


### BREAKING CHANGES

* createDefaultLogger not longer exported

# [1.1.0](https://gitlab.com/alline/core/compare/v1.0.1...v1.1.0) (2020-05-18)


### Features

* use class based plugin ([c8bdbfd](https://gitlab.com/alline/core/commit/c8bdbfd5bf2e0a5125cbd52a8b2e3af203292fb5))

## [1.0.1](https://gitlab.com/alline/core/compare/v1.0.0...v1.0.1) (2020-05-08)


### Bug Fixes

* remove script ([9fbe9f2](https://gitlab.com/alline/core/commit/9fbe9f2ef705d2667b411a83e9cad7d3ebfe4489))

# 1.0.0 (2020-05-08)


### Features

* initial commit ([2b537fe](https://gitlab.com/alline/core/commit/2b537fe616c37f52b966ba4f19d9cf229f397786))
